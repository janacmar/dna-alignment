<?php

namespace App\Controller;

use App\Form\DNAQueryType;
use App\Service\DNAFunctionality;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controls requests related to DNAs.
 *
 * @Route("/dna")
 */
class DNAController extends AbstractController
{
    /** @var DNAFunctionality */
    protected $dnaFunctionality;

    /**
     * @param DNAFunctionality $dnaFunctionality
     */
    public function __construct(DNAFunctionality $dnaFunctionality)
    {
        $this->dnaFunctionality = $dnaFunctionality;
    }

    /**
     * Resolves DNA similarity queries.
     *
     * @Route("/similarity", name="dna_similarity")
     * @param Request $request
     * @return Response
     */
    public function similarityAction(Request $request)
    {
        $form = $this->createForm(DNAQueryType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $inputDNA = $this->dnaFunctionality->getInputDNA($data);
            $databaseDNAs = $this->dnaFunctionality->getDatabaseDNAs();
            $results = [];
            if ($data['alignment'] == 'global') {
                foreach ($databaseDNAs as $databaseDNA) {
                    $results[] = $this->dnaFunctionality->NeedlemanWunsch($inputDNA, $databaseDNA);
                }
            }
            else {
                foreach ($databaseDNAs as $databaseDNA) {
                    $results[] = $this->dnaFunctionality->SmithWaterman($inputDNA, $databaseDNA);
                }
            }
            $results = $this->dnaFunctionality->sortResultsByScore($results);
            if ($data['queryType'] == 'kNN') {
                $results = array_slice($results, 0, $data['k']);
            }
            else {
                $results = $this->dnaFunctionality->filterResultsByMinScore($results, $data['minScore']);
            }
            return $this->render('dna/result.html.twig', ['results' => $results]);
        }
        return $this->render('dna/query.html.twig', ['form' => $form->createView()]);
    }
}