<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Expression;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/** DNA similarity query form. */
class DNAQueryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('input', ChoiceType::class, [
                'label' => false,
                'expanded' => true,
                'choices' => ['String input' => 'string', 'File input' => 'file'],
                'data' => 'string'
            ])
            ->add('dnaString', TextType::class, [
                'label' => 'DNA string:',
                'required' => false,
                'constraints' => [new Callback([$this, 'validateDnaString'])]
            ])
            ->add('dnaFile', FileType::class, [
                'label' => 'DNA file:',
                'required' => false,
                'constraints' => [new Callback([$this, 'validateDnaFile'])]
            ])
            ->add('queryType', ChoiceType::class, [
                'label' => false,
                'expanded' => true,
                'choices' => ['kNN query' => 'kNN', 'Range query' => 'range'],
                'data' => 'kNN'
            ])
            ->add('k', IntegerType::class, [
                'label' => 'Value of k:',
                'required' => false,
                'data' => 3,
                'constraints' => [
                    new Expression([
                        'expression' => 'this.getParent()["queryType"].getData() == "range" or 
                                         value !== "" and value !== null and value > 0'
                    ])
                ]
            ])
            ->add('minScore', IntegerType::class, [
                'label' => 'Minimal score:',
                'required' => false,
                'data' => 0,
                'constraints' => [
                    new Expression([
                        'expression' => 'this.getParent()["queryType"].getData() == "kNN" or value !== "" and value !== null'
                    ])
                ]
            ])
            ->add('alignment', ChoiceType::class, [
                'label' => false,
                'expanded' => true,
                'choices' => ['Global alignment' => 'global', 'Local alignment' => 'local'],
                'data' => 'global'
            ])
        ;
    }

    public function validateDnaString($value, ExecutionContextInterface $context)
    {
        $form = $context->getRoot();
        $data = $form->getData();
        if ($data['input'] == "string" and
            ($value === "" or $value === null or !preg_match('/^[ACGTacgt]+$/', $value))) {
            $context
                ->buildViolation('This value is not valid.')
                ->addViolation();
        }
    }

    public function validateDnaFile($value, ExecutionContextInterface $context)
    {
        $form = $context->getRoot();
        $data = $form->getData();
        if ($data['input'] == "file" and
            ($value === "" or $value === null or !preg_match('/^[ACGTacgt]+$/', file_get_contents($value)))) {
            $context
                ->buildViolation('This value is not valid.')
                ->addViolation();
        }
    }
}
