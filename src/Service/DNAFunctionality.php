<?php

namespace App\Service;

/** Helper service for DNAs. */
class DNAFunctionality
{
    public function getInputDNA($data) {
        if ($data['input'] == 'string') {
            $inputDNA = $data['dnaString'];
        }
        else {
            $inputDNA = file_get_contents($data['dnaFile']);
        }
        return strtoupper($inputDNA);
    }

    public function getDatabaseDNAs() {
        $dnas = explode('>', file_get_contents('../data/AP011173.ffn'));
        array_shift($dnas);
        foreach ($dnas as &$dna) {
            $tmp = explode("\n", $dna);
            array_shift($tmp);
            $dna = strtoupper(substr(implode($tmp), 0, 800));
        }
        unset($dna);
        return $dnas;
    }

    public function NeedlemanWunsch($inputDNA, $databaseDNA) {
        $tableWidth = strlen($inputDNA);
        $tableHeight = strlen($databaseDNA);
        $solutionTable[0][0] = ['score' => 0, 'solution' => ['upperDNA' => "", 'leftDNA' => ""]];
        for ($column = 1; $column <= $tableWidth; $column++) {
            $solutionTable[0][$column] = [
                'score' => -$column,
                'solution' => [
                    'upperDNA' => $solutionTable[0][$column - 1]['solution']['upperDNA'].substr($inputDNA, $column - 1, 1),
                    'leftDNA' => $solutionTable[0][$column - 1]['solution']['leftDNA']."-"
                ]
            ];
        }
        for ($row = 1; $row <= $tableHeight; $row++) {
            $solutionTable[$row][0] = [
                'score' => -$row,
                'solution' => [
                    'upperDNA' => $solutionTable[$row - 1][0]['solution']['upperDNA']."-",
                    'leftDNA' => $solutionTable[$row - 1][0]['solution']['leftDNA'].substr($databaseDNA, $row - 1, 1)
                ]
            ];
        }
        return $this->coreAlgorithm($inputDNA, $databaseDNA, $solutionTable, $tableWidth, $tableHeight, false);
    }

    public function SmithWaterman($inputDNA, $databaseDNA) {
        $tableWidth = strlen($inputDNA);
        $tableHeight = strlen($databaseDNA);
        $solutionTable = [];
        for ($column = 0; $column <= $tableWidth; $column++) {
            $solutionTable[0][$column] = ['score' => 0, 'solution' => ['upperDNA' => "", 'leftDNA' => ""]];
        }
        for ($row = 1; $row <= $tableHeight; $row++) {
            $solutionTable[$row][0] = ['score' => 0, 'solution' => ['upperDNA' => "", 'leftDNA' => ""]];
        }
        return $this->coreAlgorithm($inputDNA, $databaseDNA, $solutionTable, $tableWidth, $tableHeight, true);
    }

    public function sortResultsByScore($results) {
        usort($results, 'App\Service\DNAFunctionality::resultCmp');
        return $results;
    }

    public function filterResultsByMinScore($results, $minScore) {
        $results = array_filter($results, function ($var) use ($minScore) {
            return $var['score'] >= $minScore;
        });
        return $results;
    }

    /* Algorithm for similar core parts of Needleman-Wunsch and Smith-Waterman algorithms. */
    private function coreAlgorithm($inputDNA, $databaseDNA, $solutionTable, $tableWidth, $tableHeight, $SW) {
        if ($SW) {
            $bestSolution = ['score' => 0, 'solution' => ['upperDNA' => "", 'leftDNA' => ""]];
        }
        for ($row = 1; $row <= $tableHeight; $row++) {
            for ($column = 1; $column <= $tableWidth; $column++) {
                if (strcmp(substr($inputDNA, $column - 1, 1), substr($databaseDNA, $row - 1, 1)) == 0) {
                    $topleft = $solutionTable[$row - 1][$column - 1]['score'] + 1;
                }
                else {
                    $topleft = $solutionTable[$row - 1][$column - 1]['score'] - 1;
                }
                $top = $solutionTable[$row - 1][$column]['score'] - 1;
                $left = $solutionTable[$row][$column - 1]['score'] - 1;
                $max = max($top, $left, $topleft);
                if ($SW and $max <= 0) {
                    $solutionTable[$row][$column] = ['score' => 0, 'solution' => ['upperDNA' => "", 'leftDNA' => ""]];
                }
                elseif ($top == $max) {
                    $solutionTable[$row][$column] = [
                        'score' => $max,
                        'solution' => [
                            'upperDNA' => $solutionTable[$row - 1][$column]['solution']['upperDNA']."-",
                            'leftDNA' => $solutionTable[$row - 1][$column]['solution']['leftDNA'].substr($databaseDNA, $row - 1, 1)
                        ]
                    ];
                }
                elseif ($left == $max) {
                    $solutionTable[$row][$column] = [
                        'score' => $max,
                        'solution' => [
                            'upperDNA' => $solutionTable[$row][$column - 1]['solution']['upperDNA'].substr($inputDNA, $column - 1, 1),
                            'leftDNA' => $solutionTable[$row][$column - 1]['solution']['leftDNA']."-"
                        ]
                    ];
                }
                else {
                    $solutionTable[$row][$column] = [
                        'score' => $max,
                        'solution' => [
                            'upperDNA' => $solutionTable[$row - 1][$column - 1]['solution']['upperDNA'].substr($inputDNA, $column - 1, 1),
                            'leftDNA' => $solutionTable[$row - 1][$column - 1]['solution']['leftDNA'].substr($databaseDNA, $row - 1, 1)
                        ]
                    ];
                }
                if ($SW and $solutionTable[$row][$column]['score'] > $bestSolution['score']) {
                    $bestSolution = $solutionTable[$row][$column];
                }
            }
        }
        if ($SW) {
            return $bestSolution;
        }
        return $solutionTable[$tableHeight][$tableWidth];
    }

    private function resultCmp($a, $b)
    {
        if ($a['score'] == $b['score']) {
            return 0;
        }
        return ($a['score'] > $b['score']) ? -1 : 1;
    }
}